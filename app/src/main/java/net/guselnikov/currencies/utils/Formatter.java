package net.guselnikov.currencies.utils;

import android.support.annotation.NonNull;

/**
 * Helper class that contains methods for formatting strings.
 * All methods are static so there's no need to create an instance of this class.
 *
 * @author Dmitry Guselnikov.
 */
public class Formatter {

    /**
     * Formats the characteristics for displaying.
     * Example: EURUSD -> EUR / USD
     *
     * @param characteristic Characteristic to be formatted.
     * @return Formatted characteristics or unchanged input param if characteristic is
     * not 6 symbols long.
     */
    public static String formatCharacteristic(@NonNull String characteristic) {
        if (characteristic.length() != 6) {
            return characteristic;
        }

        // Here is hair thin space around the slash sign
        return characteristic.substring(0, 3) + " / " + characteristic.substring(3);
    }
}
