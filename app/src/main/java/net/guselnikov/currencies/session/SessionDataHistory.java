package net.guselnikov.currencies.session;

import android.support.annotation.NonNull;

import net.guselnikov.currencies.model.Tick;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import java.util.HashMap;

/**
 * Holds the data received in the current session. Used for collecting history data.
 *
 * @author Dmitry Guselnikov.
 */
public class SessionDataHistory {

    /**
     * Maximum number of ticks to be stored during the session.
     */
    public static final int BUFFER_SIZE = 100;

    /**
     * Container for storing data.
     * Keys are names of characteristics.
     */
    public static HashMap<String, CircularFifoQueue<Tick>> historyData = new HashMap<>();

    /**
     * Stores information about last tick.
     *
     * @param tick Tick to be stored.
     */
    public static void put(@NonNull Tick tick) {
        CircularFifoQueue<Tick> lastTicks = historyData.get(tick.symbol);
        if (lastTicks == null) {
            lastTicks = new CircularFifoQueue<>(BUFFER_SIZE);
            historyData.put(tick.symbol, lastTicks);
        }

        lastTicks.add(tick);
    }

    public static boolean hasDataForSymbol(String symbol) {
        CircularFifoQueue<Tick> data = historyData.get(symbol);
        if (data == null) {
            return false;
        }

        return data.size() > 0;
    }
}
