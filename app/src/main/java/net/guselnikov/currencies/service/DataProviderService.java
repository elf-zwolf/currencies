package net.guselnikov.currencies.service;

import android.app.Service;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import net.guselnikov.currencies.model.Tick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

/**
 * Service that connects to the server via web sockets and provides a mechanism for
 * notifying client observers that subscribed for updates.
 *
 * @author Dmitry Guselnikov.
 */
public class DataProviderService extends Service {

    /**
     * Callback interface for notifying clients about a new tick.
     *
     * @see Tick
     */
    public interface OnTickListener {
        void onTick(Tick tick);
    }

    private final class EchoWebSocketListener extends WebSocketListener {
        private static final int NORMAL_CLOSURE_STATUS = 1000;

        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            wsConnected = true;
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            try {
                JSONObject json = new JSONObject(text);
                JSONArray ticksJson = json.getJSONArray("ticks");
                if (ticksJson != null) {
                    final Tick[] ticks = new Gson().fromJson(ticksJson.toString(), Tick[].class);
                    long currentSystemTimeMillis = System.currentTimeMillis();
                    for (final Tick tick : ticks) {
                        tick.systemTime = currentSystemTimeMillis;
                        mainLoopHandler.post(() -> notifyListeners(tick));
                    }
                }

            } catch (JSONException e) {
                // if not a valid json received then return.
                e.printStackTrace();
            }
        }

        @Override
        public void onMessage(WebSocket webSocket, ByteString bytes) {
        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            wsConnected = false;
            webSocket.close(NORMAL_CLOSURE_STATUS, null);
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            wsConnected = false;
            // try to reconnect in 2.5 seconds
            pendingSubscriptions.addAll(subscriptions);
            subscriptions.clear();
            mainLoopHandler
                    .postDelayed(DataProviderService.this::connectToWebSocketAndResubscribe, 2500);
        }
    }

    OkHttpClient client = new OkHttpClient();

    /**
     * URL of data providing server.
     */
    private static final String WS_URL = "wss://quotes.exness.com/";

    /**
     * Instance of Binder that is given to service callers.
     */
    private final IBinder localBinder = new LocalBinder();

    /**
     * Web socket instance
     */
    WebSocket ws;

    /**
     * List of listeners to be notified when a new ticks are received.
     */
    private ArrayList<OnTickListener> listeners = new ArrayList<>();

    /**
     * Handler to perform actions on the UI thread.
     */
    private Handler mainLoopHandler = new Handler(Looper.getMainLooper());

    /**
     * Connection state.
     */
    private boolean wsConnected = false;

    /**
     * List of characteristics the app is currently subscribed to.
     */
    private ArrayList<String> subscriptions = new ArrayList<>();

    /**
     * List of pending characteristics in a queue to be subscribed.
     */
    private ArrayList<String> pendingSubscriptions = new ArrayList<>();

    /**
     * Adds a new listener with a callback method for ticks.
     *
     * @param listener Listener to be added in the list of subscribers.
     * @return Current instance of DataProviderService.
     */
    @NonNull
    public DataProviderService addOnTickListener(@NonNull OnTickListener listener) {
        listeners.add(listener);
        return this;
    }

    /**
     * Removes a listener that was added for receiving ticks.
     *
     * @param listener Listener to be removed from the list of subscribers.
     * @return {@code true} if listener was removed from the list, {@code false} otherwise.
     */
    public boolean removeOnTickListener(@NonNull OnTickListener listener) {
        return listeners.remove(listener);
    }

    /**
     * Subscribes for updates for given characteristic or does nothing if service is already
     * subscribed for this characteristic.
     * <p>
     * For list of available characteristics, please see characteristics array in the
     * string resources.
     *
     * @param characteristic Characteristic to be subscribed for.
     * @return Current instance of DataProviderService.
     */
    @NonNull
    public DataProviderService subscribe(String characteristic) {
        if (!subscriptions.contains(characteristic)) {
            if (!wsConnected) {
                // try to subscribe in 1s
                mainLoopHandler.postDelayed(() -> subscribe(characteristic), 1000);
            } else {
                ws.send("SUBSCRIBE: " + characteristic);
                subscriptions.add(characteristic);
            }
        }
        return this;
    }

    /**
     * Unsubscribes from the given characteristic or does nothing if service is not
     * subscribed for this characteristic.
     *
     * @param characteristic Characteristic to be unsubscribed from.
     * @return Current instance of DataProviderService.
     */
    @NonNull
    public DataProviderService unsubscribe(@NonNull String characteristic) {
        if (subscriptions.contains(characteristic)) {
            ws.send("UNSUBSCRIBE: " + characteristic);
            subscriptions.remove(characteristic);
        }
        return this;
    }

    /**
     * Notifies all the subscribers for ticks.
     */
    private void notifyListeners(Tick tick) {
        for (OnTickListener listener : listeners) {
            listener.onTick(tick);
        }
    }

    /**
     * Connects to the server using web sockets.
     */
    private void connectToWebSocket() {
        Request request = new Request.Builder().url(WS_URL).build();
        EchoWebSocketListener listener = new EchoWebSocketListener();
        ws = client.newWebSocket(request, listener);
    }

    // Reconnects and resubscribes. Call it in case of web socket error.
    private void connectToWebSocketAndResubscribe() {
        if (!isConnected()) {
            mainLoopHandler.postDelayed(this::connectToWebSocketAndResubscribe, 2500);
            return;
        }

        wsConnected = false;
        connectToWebSocket();

        // resubscribe after connect
        for (String symbol : pendingSubscriptions) {
            subscribe(symbol);
        }

        pendingSubscriptions.clear();
    }

    // Checks internet connection.
    private boolean isConnected() {
        ConnectivityManager connectivityManager = ((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE));
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    /**
     * Constructor.
     */
    public DataProviderService() { }

    @Override
    public IBinder onBind(Intent intent) {
        connectToWebSocket();
        return localBinder;
    }

    @Override
    public void onDestroy() {
        if (ws != null) {
            ws.close(1000, "Manual shutdown");
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    /**
     * Binder that returned to a callers of
     * {@code Context.bindService(Intent, ServiceConnection, int)}.
     * <p>
     * Provides an option for getting an instance of DataProviderService for clients.
     */
    public class LocalBinder extends Binder {
        public DataProviderService getServiceInstance() {
            return DataProviderService.this;
        }
    }
}
