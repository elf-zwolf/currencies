package net.guselnikov.currencies.ui;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;

import net.guselnikov.currencies.R;
import net.guselnikov.currencies.preferences.ApplicationPreferences;
import net.guselnikov.currencies.service.DataProviderService;
import net.guselnikov.currencies.ui.adapter.CharacteristicsListAdapter;

import java.util.Arrays;
import java.util.List;

/**
 * Screen that shows a list of checkboxes with names of characteristics that allow user
 * select symbols to follow on the {@link DashboardActivity} screen.
 *
 * @author Dmitry Guselnikov.
 */
public class ChooseCharacteristicsActivity extends AppCompatActivity {

    private DataProviderService dataProviderService; // Service that provides data.
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            DataProviderService.LocalBinder binder = (DataProviderService.LocalBinder) service;
            dataProviderService = binder.getServiceInstance();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) { }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_characteristics);
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final List<String> allCharacteristics =
                Arrays.asList(getResources().getStringArray(R.array.characteristics));
        final List<String> enabledCharacteristics =
                ApplicationPreferences.getInstance(this).getCharacteristics();

        final ApplicationPreferences ap = ApplicationPreferences.getInstance(this);

        final CharacteristicsListAdapter adapter = new CharacteristicsListAdapter()
                .setCharacteristics(allCharacteristics)
                .setEnabledCharacteristics(enabledCharacteristics)
                .setOnItemCheckedChangeListener((characteristic, isChecked) -> {

                            // Save enabled characteristic to ApplicationPreferences
                            if (isChecked) {
                                ap.putCharacteristic(characteristic);
                            } else {
                                ap.removeCharacteristic(characteristic);
                            }

                            // Subscribe/unsubscribe if DataProviderService
                            // is connected to the server
                            if (dataProviderService != null) {
                                if (isChecked) {
                                    dataProviderService.subscribe(characteristic);
                                } else {
                                    dataProviderService.unsubscribe(characteristic);
                                }
                            }
                        }
                );

        final ListView characteristicsListView = (ListView) findViewById(R.id.characteristic_list);
        characteristicsListView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent serviceIntent = new Intent(this, DataProviderService.class);
        startService(serviceIntent);
        bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (dataProviderService != null) {
            unbindService(serviceConnection);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
