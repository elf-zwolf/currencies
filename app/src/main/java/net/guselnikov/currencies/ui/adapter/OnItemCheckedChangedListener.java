package net.guselnikov.currencies.ui.adapter;

/**
 * Callback interface for folowing the check state of a group of checkable items.
 * Known usage in {@link CharacteristicsListAdapter}.
 *
 * @author Dmitry Guselnikov.
 */
public interface OnItemCheckedChangedListener {
    void onItemChanged(String item, boolean isChecked);
}
