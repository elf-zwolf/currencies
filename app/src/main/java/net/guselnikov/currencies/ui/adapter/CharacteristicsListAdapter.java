package net.guselnikov.currencies.ui.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import net.guselnikov.currencies.R;
import net.guselnikov.currencies.ui.ChooseCharacteristicsActivity;
import net.guselnikov.currencies.utils.Formatter;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter that processes a list of characteristics as checkable items.
 * <p>
 * All setters return the current intance of CharacteristicsListAdapter so all the set-calls
 * can be chained.
 * Can be passed to any {@link android.widget.ListView}.
 * Known usage is in {@link ChooseCharacteristicsActivity}.
 *
 * @author Dmitry Guselnikov.
 */
public class CharacteristicsListAdapter extends BaseAdapter {

    // Data
    private List<String> allCharacteristics = null;
    private List<String> enabledCharacteristics = new ArrayList<>();

    // Helper classes
    private LayoutInflater inflater = null;

    // Callbacks
    @Nullable
    private OnItemCheckedChangedListener checkedChangedListener = null;

    /**
     * Sets the list of strings that will be displayed as captions in checkboxes.
     *
     * @param characteristics Any {@link List} of {@link String}. Must not be {@code null}.
     * @return Current instance of CharacteristicsListAdapter.
     */
    @NonNull
    public CharacteristicsListAdapter setCharacteristics(@NonNull List<String> characteristics) {
        allCharacteristics = characteristics;
        return this;
    }

    /**
     * Sets a list of string that will be displayed as checked if presented in all characteristics
     * list.
     *
     * @param enabledCharacteristics Any {@link List} of {@link String}. Must not be {@code null}.
     * @return Current instance of CharacteristicsListAdapter.
     * @see #setCharacteristics(List).
     */
    public CharacteristicsListAdapter setEnabledCharacteristics(
            @NonNull List<String> enabledCharacteristics) {
        this.enabledCharacteristics = enabledCharacteristics;
        return this;
    }

    /**
     * Sets a listener for all check/uncheck events in a ListView items.
     *
     * @return Current instance of CharacteristicsListAdapter.
     */
    public CharacteristicsListAdapter setOnItemCheckedChangeListener(
            @Nullable OnItemCheckedChangedListener l) {
        checkedChangedListener = l;
        return this;
    }

    @Override
    public int getCount() {
        return allCharacteristics == null ? 0 : allCharacteristics.size();
    }

    @Override
    public Object getItem(int position) {
        return allCharacteristics.get(position);
    }

    @Override
    public long getItemId(int position) {
        return (long) position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }

        View ret;
        CheckBox checkbox;
        if (convertView == null) {
            ret = inflater.inflate(R.layout.characteristic_checkbox, parent, false);
            checkbox = (CheckBox) ret.findViewById(R.id.checkbox);
            ret.setTag(checkbox); // Just one view so we don't need a view holder class
        } else {
            ret = convertView;
            checkbox = (CheckBox) ret.getTag();
        }

        final String characteristic = allCharacteristics.get(position);
        checkbox.setText(Formatter.formatCharacteristic(characteristic));
        checkbox.setChecked(enabledCharacteristics.contains(characteristic));

        checkbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                enabledCharacteristics.add(characteristic);
            } else {
                enabledCharacteristics.remove(characteristic);
            }

            if (checkedChangedListener != null) {
                checkedChangedListener.onItemChanged(characteristic, isChecked);
            }

            notifyDataSetChanged();
        });

        return ret;
    }
}
