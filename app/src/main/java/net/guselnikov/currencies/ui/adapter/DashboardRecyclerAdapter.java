package net.guselnikov.currencies.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import net.guselnikov.currencies.R;
import net.guselnikov.currencies.model.Tick;
import net.guselnikov.currencies.utils.Formatter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Adapter for {@link RecyclerView} that is used for displaying a dynamic table with data.
 *
 * @author Dmitry Guselnikov.
 */
public class DashboardRecyclerAdapter extends RecyclerView.Adapter<DashboardRecyclerAdapter.ViewHolder> {

    /**
     * Callback interface for item click events.
     */
    public interface OnItemClickListener {
        /**
         * Gets fired when a row clicked.
         *
         * @param characteristic Value of the 'symbol' column of the clicked row.
         */
        void onItemClick(String characteristic);
    }

    private static final long FLASH_ANIMATION_DURATION_MILLIS = 800L;

    // Last ticks to be displayed. Useful when no data available.
    private static HashMap<String, Tick> lastTicks = new HashMap<>();

    // Context of RecyclerView
    private Context context;

    // List of names of enabled characteristics
    private ArrayList<String> data = new ArrayList<>();

    private LayoutInflater inflater;

    /**
     * Callback for item clicks.
     */
    private OnItemClickListener onItemClickListener = null;

    /**
     * Sets a listener for row click events.
     *
     * @param l Callback for item click events.
     * @return Current instance of DashboardRecyclerAdapter.
     */
    public DashboardRecyclerAdapter setOnItemClickListener(OnItemClickListener l) {
        onItemClickListener = l;
        return this;
    }

    /**
     * Sets a collection of {@link String} to adapter.
     * <p>
     * If the view using DashboardRecyclerAdapter is already shown then call
     * {@link #notifyDataSetChanged()} after setting data.
     *
     * @param characteristics Any collection of strings.
     * @return Current instance of DashboardRecyclerAdapter.
     */
    @NonNull
    public DashboardRecyclerAdapter setData(Collection<? extends String> characteristics) {
        if (characteristics != null) {
            data = new ArrayList<>();
            data.addAll(characteristics);
        } else {
            data = null;
        }

        return this;
    }

    /**
     * Removes an element from adapter.
     * <p>
     * If the view using DashboardRecyclerAdapter is already shown then call
     * {@link #notifyItemRemoved(int)} after removing an element.
     *
     * @param position Position of element that is going to be removed.
     * @return Removed element.
     */
    @NonNull
    public String remove(int position) {
        return data.remove(position);
    }

    /**
     * Moves an element from oldElementPosition to newElementPosition.
     * <p>
     * If the view using DashboardRecyclerAdapter is already shown then call
     * {@link #notifyItemMoved(int, int)} after changing position of the element.
     *
     * @return List after moving the element.
     */
    @NonNull
    public List<String> move(int oldElementPosition, int newElementPosition) {

        if (oldElementPosition < newElementPosition) {
            for (int i = oldElementPosition; i < newElementPosition; i++) {
                Collections.swap(data, i, i + 1);
            }
        } else {
            for (int i = oldElementPosition; i > newElementPosition; i--) {
                Collections.swap(data, i, i - 1);
            }
        }
        return data;
    }

    /**
     * Updates characteristic given in tick.symbol with data provided in the rest
     * of Tick fields.
     *
     * @param tick Tick for characteristic.
     * @return Current instance of DashboardRecyclerAdapter.
     */
    public DashboardRecyclerAdapter tick(Tick tick) {
        int position = data.indexOf(tick.symbol);

        if (position != -1) {
            lastTicks.put(data.get(position), tick);
            notifyItemChanged(position);
        }

        return this;
    }

    /**
     * Returns position of given characteristic or -1 if no characteristic found.
     */
    public int getPositionByCharacteristic(@NonNull String characteristic) {
        return data.indexOf(characteristic);
    }

    /**
     * Sets context of ListView that uses this adapter.
     *
     * @param context Context of RecyclerView.
     * @return Current instance of DashboardRecyclerAdapter.
     */
    @NonNull
    public DashboardRecyclerAdapter setContext(@NonNull Context context) {
        this.context = context;
        return this;
    }

    @NonNull
    private LayoutInflater getInflater() {
        if (inflater == null) {
            inflater = LayoutInflater.from(context);
        }

        return inflater;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = getInflater().inflate(R.layout.item_table_row, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String characteristic = data.get(position);
        holder.symbol.setText(Formatter.formatCharacteristic(characteristic));

        Tick tick = lastTicks.get(characteristic);

        if (tick == null) {
            holder.bidask.setText("—");
            holder.spread.setText("—");
        } else {
            String pattern = tick.bid >= 1.0 ? "%.6g" : "%.5g";
            @SuppressLint("DefaultLocale") String bidStr = String.format(pattern, tick.bid);
            @SuppressLint("DefaultLocale") String askStr = String.format(pattern, tick.ask);
            holder.bidask.setText(bidStr + " / " + askStr);
            holder.spread.setText(String.valueOf(tick.spread));
            flash(holder.flashOverlay);
        }

        holder.itemView.setClickable(true);
        holder.itemView.setOnClickListener(v -> {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(characteristic);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (data == null) {
            return 0;
        }

        return data.size();
    }

    // Flashes the just updated row.
    private void flash(final View view) {
        Animation flash = AnimationUtils.loadAnimation(context, R.anim.flash_fade_out);
        flash.setDuration(FLASH_ANIMATION_DURATION_MILLIS);
        view.startAnimation(flash);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View rootView) {
            super(rootView);

            symbol = (TextView) rootView.findViewById(R.id.symbol);
            bidask = (TextView) rootView.findViewById(R.id.bidask);
            spread = (TextView) rootView.findViewById(R.id.spread);
            flashOverlay = rootView.findViewById(R.id.flash_overlay);
        }

        TextView symbol;
        TextView bidask;
        TextView spread;
        View flashOverlay;
    }
}
