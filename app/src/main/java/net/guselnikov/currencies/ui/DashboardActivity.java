package net.guselnikov.currencies.ui;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Canvas;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import net.guselnikov.currencies.R;
import net.guselnikov.currencies.preferences.ApplicationPreferences;
import net.guselnikov.currencies.service.DataProviderService;
import net.guselnikov.currencies.session.SessionDataHistory;
import net.guselnikov.currencies.ui.adapter.DashboardRecyclerAdapter;

import java.util.List;

/**
 * Dashboard screen. Shows a table with real-time currency rates.
 * List of shown characteristics is set in {@link ChooseCharacteristicsActivity}.
 *
 * @author Dmitry Guselnikov.
 */
public class DashboardActivity extends AppCompatActivity {

    // Views
    private View emptyStateView; // Shown when no characteristics were enabled.
    private RecyclerView recyclerView; // View for displaying list of characteristics.
    private View tableHeader; // Header of the table always connected to the top of the screen.

    // Data, controllers, providers
    private static final long MINIMAL_MOVEMENT_DELAY_MILLIS = 450L;
    private List<String> enabledCharacteristics; // Chosen characteristics for following.
    private DashboardRecyclerAdapter adapter; // Adapter for RecyclerView.
    private DataProviderService dataProviderService; // Service that provides data.

    // Time of last view movement in UNIX-time.
    // Used for deciding if we need to update a view.
    // Workaround for getting rid of conflicts in updating views while they're
    // being rearranged or swiped.
    private long lastMovement = 0L;

    // Handler for connect/disconnect events.
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            DataProviderService.LocalBinder binder = (DataProviderService.LocalBinder) service;
            dataProviderService = binder.getServiceInstance();
            dataProviderService.addOnTickListener(onTickListener);

            if (enabledCharacteristics != null) {
                for (String characteristic : enabledCharacteristics) {
                    dataProviderService.subscribe(characteristic);
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) { }
    };

    // Listener for tick updates.
    private DataProviderService.OnTickListener onTickListener = tick -> {
        if (System.currentTimeMillis() - lastMovement > MINIMAL_MOVEMENT_DELAY_MILLIS) {
            adapter.tick(tick);
        }

        SessionDataHistory.put(tick);
    };

    // Inits touch events handling (swipe and drag)
    private void initSwipeHandling() {
        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN,
                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                    @Override
                    public void onChildDraw(Canvas c,
                                            RecyclerView recyclerView,
                                            RecyclerView.ViewHolder viewHolder,
                                            float dX,
                                            float dY,
                                            int actionState,
                                            boolean isCurrentlyActive) {

                        // if view is currently being moved then
                        // remember last movement time
                        if (Math.abs(dX) > 1f || Math.abs(dY) > 1f) {
                            lastMovement = System.currentTimeMillis();
                        }

                        super.onChildDraw(c,
                                recyclerView,
                                viewHolder,
                                dX,
                                dY,
                                actionState,
                                isCurrentlyActive);
                    }

                    @Override
                    public boolean isLongPressDragEnabled() {
                        return true;
                    }

                    @Override
                    public boolean isItemViewSwipeEnabled() {
                        return true;
                    }

                    @Override
                    public boolean onMove(RecyclerView recyclerView,
                                          RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        int from = viewHolder.getAdapterPosition();
                        int to = target.getAdapterPosition();
                        List<String> characteristics = adapter.move(from, to);
                        adapter.notifyItemMoved(from, to);

                        // save a new ordered list to preferences
                        ApplicationPreferences.getInstance(DashboardActivity.this)
                                .setCharacteristics(characteristics);
                        return true;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                        int position = viewHolder.getAdapterPosition();
                        String removedItem = adapter.remove(position);
                        adapter.notifyItemRemoved(position);

                        ApplicationPreferences.getInstance(DashboardActivity.this)
                                .removeCharacteristic(removedItem);

                        if (adapter.getItemCount() == 0) {
                            showEmptyStateView(true);
                        }

                        dataProviderService.unsubscribe(removedItem);
                    }
                };

        ItemTouchHelper helper = new ItemTouchHelper(simpleCallback);
        helper.attachToRecyclerView(recyclerView);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        ApplicationPreferences ap = ApplicationPreferences.getInstance(this);
        enabledCharacteristics = ap.getCharacteristics();

        recyclerView = (RecyclerView) findViewById(R.id.characteristic_list);
        recyclerView.setHasFixedSize(true); // for performance reasons
        adapter = new DashboardRecyclerAdapter()
                .setContext(this)
                .setOnItemClickListener(characteristic -> {
                    if (SessionDataHistory.hasDataForSymbol(characteristic)) {
                        Intent intent = new Intent(this, ChartActivity.class);
                        intent.putExtra(ChartActivity.EXTRA_CHARACTERISTIC, characteristic);
                        startActivity(intent);
                    }
                });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        initSwipeHandling();

        emptyStateView = findViewById(R.id.empty_state);
        tableHeader = findViewById(R.id.table_header);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Update the list of enabled characteristics
        ApplicationPreferences ap = ApplicationPreferences.getInstance(this);
        enabledCharacteristics = ap.getCharacteristics();
        boolean isEmpty = enabledCharacteristics.isEmpty();
        showEmptyStateView(isEmpty);

        if (!isEmpty) {
            adapter.setData(enabledCharacteristics).notifyDataSetChanged();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent serviceIntent = new Intent(this, DataProviderService.class);
        startService(serviceIntent);
        bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (dataProviderService != null) {
            unbindService(serviceConnection);
            dataProviderService.removeOnTickListener(onTickListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.dashboard_screen_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.adjust_characteristics) {
            startActivity(new Intent(this, ChooseCharacteristicsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Shows/hides empty state view.
    private void showEmptyStateView(boolean show) {
        if (show) {
            emptyStateView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            tableHeader.setVisibility(View.GONE);
        } else {
            emptyStateView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            tableHeader.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Starts {@link ChooseCharacteristicsActivity}.
     * Called from onClick attribute of xml-layout.
     * Don't call it manually.
     *
     * @param view Clicked view.
     */
    public void chooseCharacteristics(View view) {
        startActivity(new Intent(this, ChooseCharacteristicsActivity.class));
    }
}
