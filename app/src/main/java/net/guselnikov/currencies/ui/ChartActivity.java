package net.guselnikov.currencies.ui;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.IBinder;
import android.support.annotation.ColorInt;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import net.guselnikov.currencies.R;
import net.guselnikov.currencies.model.Tick;
import net.guselnikov.currencies.service.DataProviderService;
import net.guselnikov.currencies.session.SessionDataHistory;
import net.guselnikov.currencies.utils.Formatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Class for displaying bid/ask charts for characteristic.
 * <p>
 * Charts are based on MPAndroidCharts library.
 * https://github.com/PhilJay/MPAndroidChart
 *
 * @author Dmitry Guselnikov.
 */
public class ChartActivity extends AppCompatActivity {

    /**
     * Extra activity param for name of characteristic.
     */
    public static final String EXTRA_CHARACTERISTIC = "characteristic";

    /**
     * Constant that holds period of whole chart invalidation.
     */
    public int INVALIDATE_PERIOD = 25;

    /**
     * Space in milliseconds for default zoom.
     */
    public static final int MAX_SPACE = 40000;

    /**
     * Characteristic to follow.
     */
    private String characteristic;

    /**
     * Controller for handling chart drawing.
     */
    private LineChart chart;

    // Chart helper containers
    LineDataSet bidsDataSet;
    LineDataSet asksDataSet;

    // Data
    private ArrayList<Double> bids = new ArrayList<>();
    private ArrayList<Double> asks = new ArrayList<>();
    private ArrayList<Tick> tickList = new ArrayList<>();
    // borders of the chart
    double maxBid = 0.0;
    double minBid = Double.MAX_VALUE;
    double maxAsk = 0.0;
    double minAsk = Double.MAX_VALUE;

    private DataProviderService dataProviderService; // Service that provides data.
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            DataProviderService.LocalBinder binder = (DataProviderService.LocalBinder) service;
            dataProviderService = binder.getServiceInstance();
            dataProviderService.addOnTickListener(onTickListener);
            dataProviderService.subscribe(characteristic);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) { }
    };
    private DataProviderService.OnTickListener onTickListener = tick -> {
        if (!tick.symbol.equals(characteristic)) {
            SessionDataHistory.put(tick);
            return;
        }

        tickList.add(tick);
        bids.add(tick.bid);
        asks.add(tick.ask);
        boolean changeYAxis = false;

        if (maxBid < tick.bid) {
            maxBid = tick.bid;
            changeYAxis = true;
        }

        if (minBid > tick.bid) {
            minBid = tick.bid;
            changeYAxis = true;
        }

        if (maxAsk < tick.ask) {
            maxAsk = tick.ask;
            changeYAxis = true;
        }

        if (minAsk > tick.ask) {
            minAsk = tick.ask;
            changeYAxis = true;
        }

        if (changeYAxis) {
            YAxis leftAxis = chart.getAxisLeft();
            leftAxis.setAxisMaximum(((float) Math.max(maxBid, maxAsk)) * 1.0001f); // add padding-top
            leftAxis.setAxisMinimum(((float) Math.min(minBid, minAsk)) * 0.9999f); // add padding-bottom
        }

        float x = 0f;
        if (!tickList.isEmpty()) {
            x = (tick.systemTime - tickList.get(0).systemTime);
        }

        asksDataSet.addEntry(new Entry(x, (float) tick.ask));
        bidsDataSet.addEntry(new Entry(x, (float) tick.bid));
        asksDataSet.notifyDataSetChanged();
        bidsDataSet.notifyDataSetChanged();

        if (tickList.size() % INVALIDATE_PERIOD == 0) {
            chart.clear();
            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(asksDataSet);
            dataSets.add(bidsDataSet);
            LineData data = new LineData(dataSets);
            chart.setData(data);

            Legend l = chart.getLegend();
            l.setForm(Legend.LegendForm.LINE);
        } else {
            chart.notifyDataSetChanged();
            chart.invalidate();
        }

        SessionDataHistory.put(tick);
    };

    private void initBidAskArrays() {
        Iterable<Tick> ticks = SessionDataHistory.historyData.get(characteristic);
        for (Tick tick : ticks) {
            tickList.add(tick);
            bids.add(tick.bid);
            asks.add(tick.ask);

            if (maxBid < tick.bid) {
                maxBid = tick.bid;
            }

            if (minBid > tick.bid) {
                minBid = tick.bid;
            }

            if (maxAsk < tick.ask) {
                maxAsk = tick.ask;
            }

            if (minAsk > tick.ask) {
                minAsk = tick.ask;
            }
        }
    }

    /**
     * Initializes the data for chart.
     */
    private void initChart() {
        initBidAskArrays();

        chart = (LineChart) findViewById(R.id.chart);

        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        chart.setBackgroundColor(Color.WHITE);
        chart.getDescription().setEnabled(false); // Disable chart description
        chart.setPinchZoom(false); // x-only or y-only at once

        XAxis xAxis = chart.getXAxis();
        xAxis.setSpaceMax(MAX_SPACE);

        float dashLendth = getResources().getDimension(R.dimen.chart_dash_length);
        float dashSpacing = getResources().getDimension(R.dimen.chart_dash_spacing);
        xAxis.enableGridDashedLine(dashLendth, dashSpacing, 0f);
        xAxis.setGranularity(1000f);

        final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        xAxis.setValueFormatter((value, axis) -> {
            if (tickList.isEmpty()) {
                return "";
            }

            long millis = tickList.get(0).systemTime + (long) value;
            return timeFormat.format(new Date(millis));
        });

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines

        leftAxis.setAxisMaximum(((float) Math.max(maxBid, maxAsk)) * 1.0001f); // add padding-top
        leftAxis.setAxisMinimum(((float) Math.min(minBid, minAsk)) * 0.9999f); // add padding-bottom
        leftAxis.enableGridDashedLine(dashLendth, dashSpacing, 0f);

        leftAxis.setDrawLimitLinesBehindData(true);

        chart.getAxisRight().setEnabled(false);
        chart.clear();

        bidsDataSet = initBidsData();
        asksDataSet = initAsksData();

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(asksDataSet);
        dataSets.add(bidsDataSet);
        LineData data = new LineData(dataSets);
        chart.setData(data);

        Legend l = chart.getLegend();
        l.setForm(Legend.LegendForm.LINE);
    }

    /**
     * Initializes data for Asks graph.
     *
     * @return LineDataSet instance to be passed to chart.
     */
    private LineDataSet initAsksData() {
        ArrayList<Entry> askEntries = new ArrayList<>();

        for (int i = 0; i < asks.size(); i++) {

            float x = 0f;
            if (!tickList.isEmpty()) {
                x = (tickList.get(i).systemTime - tickList.get(0).systemTime);
            }

            askEntries.add(new Entry(x, asks.get(i).floatValue()));
        }

        LineDataSet asksDataSet;

        // create a dataset and give it a type
        asksDataSet = new LineDataSet(askEntries, "ASK");

        float datalineDashLength = getResources()
                .getDimension(R.dimen.chart_dataline_dash_lendth);

        float datalineDashSpacing = getResources()
                .getDimension(R.dimen.chart_dataline_dash_spacing);

        float datalineWidth = getResources()
                .getDimension(R.dimen.chart_dataline_width);

        float datalineCircleRaduis = getResources()
                .getDimension(R.dimen.chart_dataline_cirle_radius);

        @ColorInt int askColor = getResources().getColor(R.color.askDatalineColor);

        asksDataSet.enableDashedLine(datalineDashLength, datalineDashSpacing, 0f);
        asksDataSet.enableDashedHighlightLine(datalineDashLength, datalineDashSpacing, 0f);
        asksDataSet.setColor(askColor);
        asksDataSet.setCircleColor(askColor);
        asksDataSet.setLineWidth(datalineWidth);
        asksDataSet.setCircleRadius(datalineCircleRaduis);
        asksDataSet.setDrawFilled(true);
        asksDataSet.setDrawValues(false);
        asksDataSet.setFillColor(Color.WHITE);

        return asksDataSet;
    }

    /**
     * Initializes data for Bids graph.
     *
     * @return LineDataSet instance to be passed to chart.
     */
    private LineDataSet initBidsData() {

        ArrayList<Entry> bidEntries = new ArrayList<>();

        for (int i = 0; i < bids.size(); i++) {

            float x = 0f;
            if (!tickList.isEmpty()) {
                x = (tickList.get(i).systemTime - tickList.get(0).systemTime);
            }

            bidEntries.add(new Entry(x, bids.get(i).floatValue()));
        }

        LineDataSet bidsDataSet;

        // create a dataset and give it a type
        bidsDataSet = new LineDataSet(bidEntries, "BID");

        float datalineDashLength = getResources()
                .getDimension(R.dimen.chart_dataline_dash_lendth);

        float datalineDashSpacing = getResources()
                .getDimension(R.dimen.chart_dataline_dash_spacing);

        float datalineWidth = getResources()
                .getDimension(R.dimen.chart_dataline_width);

        float datalineCircleRaduis = getResources()
                .getDimension(R.dimen.chart_dataline_cirle_radius);

        @ColorInt int bidColor = getResources().getColor(R.color.bidDatalineColor);

        bidsDataSet.enableDashedLine(datalineDashLength, datalineDashSpacing, 0f);
        bidsDataSet.enableDashedHighlightLine(datalineDashLength, datalineDashSpacing, 0f);
        bidsDataSet.setColor(bidColor);
        bidsDataSet.setCircleColor(bidColor);
        bidsDataSet.setLineWidth(datalineWidth);
        bidsDataSet.setCircleRadius(datalineCircleRaduis);
        bidsDataSet.setDrawFilled(true);
        bidsDataSet.setDrawValues(false);
        bidsDataSet.setFillColor(Color.WHITE);

        return bidsDataSet;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        if (!getIntent().hasExtra(EXTRA_CHARACTERISTIC)) {
            throw new RuntimeException("Invalid params");
        }

        characteristic = getIntent().getStringExtra(EXTRA_CHARACTERISTIC);

        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Formatter.formatCharacteristic(characteristic) + " chart");
        initChart();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent serviceIntent = new Intent(this, DataProviderService.class);
        startService(serviceIntent);
        bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (dataProviderService != null) {
            unbindService(serviceConnection);
            dataProviderService.removeOnTickListener(onTickListener);
        }
    }
}
