package net.guselnikov.currencies.model;

import com.google.gson.annotations.SerializedName;

/**
 * Model class for web socket updates.
 *
 * @author Dmitry Guselnikov.
 */
public class Tick {
    /**
     * Name of characteristic.
     */
    @SerializedName("s")
    public String symbol;

    /**
     * Bid value for characteristic for certain moment.
     */
    @SerializedName("b")
    public double bid;

    /**
     * Ask value for characteristic for certain moment.
     */
    @SerializedName("a")
    public double ask;

    /**
     * Spread value for characteristic for certain moment.
     */
    @SerializedName("spr")
    public double spread;

    /**
     * Time of receiving tick.
     */
    public long systemTime;
}
