package net.guselnikov.currencies.preferences;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.AnyThread;
import android.support.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Class that handles all the app preferences. Singleton.
 * <p>
 * All Create, Update and Delete (set, put and remove) methods return current instance
 * of the class so these methods can be chained.
 * Implementation is based on {@link SharedPreferences}.
 *
 * @author Dmitry Guselnikov
 */
public class ApplicationPreferences {

    private static final String PREFERENCES_NAME = "preferences";
    private static final String PREFERENCE_CHARS = "chars";

    private static ApplicationPreferences instance;

    private JSONArray fromList(List<String> list) {
        JSONArray ret = new JSONArray();
        for (String s : list) {
            ret.put(s);
        }

        return ret;
    }

    private List<String> toList(JSONArray array) throws JSONException {
        List<String> ret = new ArrayList<>();
        for (int i = 0; i < array.length(); ++i) {
            ret.add((String) array.get(i));
        }

        return ret;
    }

    /**
     * Returns single instance of the class.
     */
    @AnyThread
    @NonNull
    public static ApplicationPreferences getInstance(@NonNull Context context) {
        if (instance == null) {
            instance = new ApplicationPreferences(context.getApplicationContext());
        }

        return instance;
    }

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @SuppressLint("CommitPrefEdits")
    private void init(Context context) {
        preferences = context.getSharedPreferences(PREFERENCES_NAME, 0);
        editor = preferences.edit();
    }

    // Private constructor used only inside of getInstance()
    private ApplicationPreferences(Context context) {
        init(context);
    }

    /**
     * Saves the given characteristic as checked.
     * Known usage is in {@link net.guselnikov.currencies.ui.ChooseCharacteristicsActivity}.
     *
     * @param characteristic A characteristic to be saved.
     * @return Current instance of ApplicationPreferences.
     */
    @AnyThread
    @NonNull
    public ApplicationPreferences putCharacteristic(@NonNull String characteristic) {
        List<String> chars = getCharacteristics();
        chars.add(characteristic);
        editor.putString(PREFERENCE_CHARS, fromList(chars).toString()).apply();
        return this;
    }

    /**
     * Removes the given characteristic from the list of checked ones. If the characteristic is
     * not in the list then method does not affect the stored data.
     * Known usage is in {@link net.guselnikov.currencies.ui.ChooseCharacteristicsActivity}.
     *
     * @param characteristic Characteristic to be removed from the stored list.
     * @return Current instance of ApplicationPreferences.
     */
    @AnyThread
    @NonNull
    public ApplicationPreferences removeCharacteristic(@NonNull String characteristic) {
        List<String> chars = getCharacteristics();
        if (chars.remove(characteristic)) { // remove(Object o) returns true if collection changed
            editor.putString(PREFERENCE_CHARS, fromList(chars).toString()).apply();
        }

        return this;
    }

    /**
     * Saves any collection of characteristics to the list of checked ones. Rewrites the current
     * list in one was saved before.
     *
     * @param characteristics Collection of characteristics to be saved.
     * @return Current instance of ApplicationPreferences.
     */
    @AnyThread
    @NonNull
    public ApplicationPreferences setCharacteristics(@NonNull Collection<String> characteristics) {
        List<String> listToBeSaved = new ArrayList<>(characteristics);
        editor.putString(PREFERENCE_CHARS, fromList(listToBeSaved).toString()).apply();

        return this;
    }

    /**
     * Returns list of saved characteristics. If no characteristic was saved then an empty set
     * is returned.
     */
    @NonNull
    public List<String> getCharacteristics() {
        try {
            JSONArray characteristicsArray = new JSONArray(preferences.getString(PREFERENCE_CHARS, "[]"));
            return toList(characteristicsArray);
        } catch (JSONException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

}
